#!/usr/bin/env /bin/bash

RUID=${RUID:-10001}
RGID=${RGID:-10001}
WDIR=${WDIR:-/opt/runner}

usrmod=( usermod )
grpmod=( groupmod )

[[ $(id -u runner) != "${RUID}" ]] && usrmod+=( --non-unique --uid "${RUID}" )
[[ $(id -g runner) != "${RGID}" ]] && usrmod+=( --gid "${RGID}" )  && grpmod+=( --non-unique --gid "${RGID}" )
[[ $(awk -F: '/runner/{print $6}' /etc/passwd) != "${WDIR}" ]]     && usrmod+=( --home "${WDIR}" --move-home )
[[ $(awk -F: '/runner/{print $7}' /etc/passwd) != /sbin/nologin ]] && usrmod+=( --shell '/bin/false' )

[[ "${grpmod[*]}" != groupmod ]] && eval "${grpmod[*]}" runner
[[ "${usrmod[*]}" != usermod ]]  && eval "${usrmod[*]}" runner

if [[ ${INIT_UPDATE_PERMS:=true} =~ ^(true|false)$ ]] && ${INIT_UPDATE_PERMS};then
    chown -cR runner:runner "${WDIR}"
    chmod -cR ug+rwX,o+rX-w "${WDIR}"
fi
