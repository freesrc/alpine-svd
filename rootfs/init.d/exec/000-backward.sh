#!/usr/bin/env /bin/bash

if [[ -d /exec ]]; then
    [[ -d /exec/init.d ]]    && mv /exec/init.d/* /init.d/exec/
    [[ -d /exec/env.d ]]     && mv /exec/env.d/* /init.d/env/
    [[ -d /exec/install.d ]] && mv /exec/install.d/* /init.d/install/
    rm -rf /exec/{init.d,env.d,install.d}

    for i in /exec/*; do
        mv "${i}" /init.d/
        rm -rf "${i}"
    done

    rm -rf /exec

    kill -HUP 1
fi
