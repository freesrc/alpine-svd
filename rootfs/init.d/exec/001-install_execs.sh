#!/usr/bin/env /bin/bash

[[ ! -d /usr/local/bin ]]  && mkdir -p /usr/local/bin
[[ ! -d /usr/local/sbin ]] && mkdir -p /usr/local/sbin

if [[ -d /init.d/install ]];then
    for bin in /init.d/install/*;do
        if [[ -f "${bin}" && "${bin}" =~ ^.*\.((s)?bin)$ ]];then
            chmod +x "${bin}"
            ln -sfv "$(realpath "${bin}")" /usr/local/"${BASH_REMATCH[1]}"/"$(basename "${bin%.*}")"
        fi
    done
fi
