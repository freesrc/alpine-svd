proxy_connect_timeout    60s;  # Задаёт таймаут для установления соединения с проксированным сервером.
proxy_send_timeout       60s;  # Задаёт таймаут при передаче запроса проксированному серверу.
proxy_read_timeout       60s;  # Задаёт таймаут при чтении ответа проксированного сервера.

proxy_max_temp_file_size 0;
proxy_buffering          on;
proxy_buffer_size        8k;
proxy_buffers            2048 8k;
