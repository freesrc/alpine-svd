add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;      # Enable HSTS
add_header X-Frame-Options           SAMEORIGIN;                                        # Deny load frames from outside
add_header X-Content-Type-Options    nosniff;                                           # Protect against MIME sniffing vulnerabilities
add_header X-XSS-Protection          "1; mode=block";                                   # Enable XSS protection
add_header Cache-Control             "max-age=0, no-cache, no-store, must-revalidate";  # Disable browsers cache
add_header Pragma                    "no-cache";                                        # Disable HTTP/1.0 browsers cache
