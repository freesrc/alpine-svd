gzip            on;
gzip_disable    "msie6";
gzip_vary       on;
gzip_comp_level 6;
gzip_min_length 256;
gzip_proxied    any;
gzip_types      application/atom+xml text/javascript application/javascript application/x-javascript application/json application/ld+json application/manifest+json application/vnd.geo+json application/vnd.ms-fontobject application/wasm application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml text/xml application/xml font/opentype image/bmp image/svg+xml application/rss+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;
