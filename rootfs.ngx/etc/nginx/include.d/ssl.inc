resolver                  8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout          10s;

ssl_protocols             TLSv1.3 TLSv1.2 TLSv1.1;
ssl_ciphers               ECDHE:+AES256:-3DES:RSA+AES:!NULL:!RC4;
ssl_prefer_server_ciphers on;

ssl_buffer_size           8k;

ssl_stapling              on;
ssl_stapling_verify       on;
