#!/usr/bin/env /bin/bash

if [[ ${NGXLOGERR} ]];then
    sed -ri "s/(error_log.* )[^[:space:]]*;/\1${NGXLOGERR};/g;" /etc/nginx/nginx.conf
fi

if [[ ${REVERSE_PROXY} ]];then
    sed -i "s/{{ REVERSE_PROXY }}/${REVERSE_PROXY}/g;" /etc/nginx/conf.d/default.conf
else
    sed -i '/^geoip_proxy.*$/d' /etc/nginx/conf.d/default.conf
fi
