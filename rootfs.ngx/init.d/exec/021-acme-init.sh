#!/usr/bin/env /bin/bash
if [[ ${ACMESH_ENABLE} =~ ^(true|false)$ ]] && ${ACMESH_ENABLE};then
    /opt/acme/acme.sh --home "${LE_WORKING_DIR}" --upgrade --auto-upgrade
    if [[ -n ${ACME_ACCOUNT_MAIL} ]]; then
        if [[ -f ${LE_WORKING_DIR}/ca/acme-v02.api.letsencrypt.org/account.key ]]; then
            /opt/acme/acme.sh --home "${LE_WORKING_DIR}" \
                              --updateaccount \
                              --accountkeylength "${ACME_ACCOUNT_KEY_LENGTH}" \
                              --accountemail "${ACME_ACCOUNT_MAIL}"
        else
            /opt/acme/acme.sh --home "${LE_WORKING_DIR}" \
                              --registeraccount \
                              --accountkeylength "${ACME_ACCOUNT_KEY_LENGTH}" \
                              --accountemail "${ACME_ACCOUNT_MAIL}"
        fi
    fi
    /opt/acme/acme.sh --home "${LE_WORKING_DIR}" --renewAll

    for WEBCONF in /etc/nginx/conf.d/*.conf
    do
        webdmn=$(sed -rn 's|^[#[:space:]]*acme.sh_domain=([a-z0-9.-]*).*$|\1|p;' "${WEBCONF}")
        webcrt=$(sed -rn 's|^[#[:space:]]*ssl_certificate[^_][[:space:]]*(.*);|\1|p;' "${WEBCONF}")
        webkey=$(sed -rn 's|^[#[:space:]]*ssl_certificate_key[[:space:]]*(.*);|\1|p;' "${WEBCONF}")

        if [[ -n ${webdmn} && -d ${LE_WORKING_DIR}/${webdmn} && ( ! -f ${webcrt} || ! -f ${webkey} ) ]]; then
            /opt/acme/acme.sh -d "${webdmn}" \
                              --installcert \
                              --fullchainpath "${webcrt}" \
                              --keypath       "${webkey}" \
                              --reloadcmd 'nginx -s reload'
        fi
        unset webdmn webcrt webkey
    done
    unset WEBCONF
fi
