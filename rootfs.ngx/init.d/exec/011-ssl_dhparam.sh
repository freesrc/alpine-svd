#!/usr/bin/env /bin/bash

[[ ! -f /etc/nginx/ssl/dhparam2048 ]] && /usr/bin/openssl dhparam -out /etc/nginx/ssl/dhparam2048 -dsaparam 2048
[[ ! -f /etc/nginx/ssl/dhparam4096 ]] && /usr/bin/openssl dhparam -out /etc/nginx/ssl/dhparam4096 -dsaparam 4096
