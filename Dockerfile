ARG         ALPINE_VERSION
FROM        alpine:${ALPINE_VERSION} AS python-bld
ARG         PYTHON_VERSION=3.12.9

RUN         set -eux; \
            apk add --no-cache --virtual .build-deps \
                curl \
                tar \
                xz \
                bluez-dev \
                bzip2-dev \
                dpkg-dev dpkg \
                findutils \
                gcc \
                gdbm-dev \
                libc-dev \
                libffi-dev \
                libnsl-dev \
                libtirpc-dev \
                linux-headers \
                make \
                ncurses-dev \
                openssl-dev \
                pax-utils \
                readline-dev \
                sqlite-dev \
                tcl-dev \
                tk \
                tk-dev \
                util-linux-dev \
                xz-dev \
                zlib-dev \
            ;

RUN         set -eux; \
                curl -fS http://storage.svc.freesrc.ru/python/${PYTHON_VERSION}.tar.xz \
                    -o /tmp/python.tar.xz
RUN         set -eux; \
                echo "$(curl -fsSL http://storage.svc.freesrc.ru/python/${PYTHON_VERSION}.sum.sha256) */tmp/python.tar.xz" | sha256sum -c
RUN         mkdir -p /usr/src/python
RUN         tar -xC /usr/src/python -f /tmp/python.tar.xz --strip-components=1
ENV         EXTRA_CFLAGS="-DTHREAD_STACK_SIZE=0x100000" \
            LDFLAGS="-Wl,--strip-all"
RUN         set -eux ; \
                ( \
                    cd /usr/src/python ; \
                    ./configure \
                        --build="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)" \
                        --enable-loadable-sqlite-extensions \
                        --enable-option-checking=fatal \
                        --enable-shared \
                        --with-lto \
                        --with-ensurepip \
                    ; \
                    case "$(apk --print-arch)" in \
                        'x86_64|aarch64') \
                            EXTRA_CFLAGS="${EXTRA_CFLAGS:-} -fno-omit-frame-pointer -mno-omit-leaf-frame-pointer" \
                        ;; \
                        'x86')    ;; \
                        *) \
                            EXTRA_CFLAGS="${EXTRA_CFLAGS:-} -fno-omit-frame-pointer" \
                        ;; \
                    esac \
                    ; \
                    make -j "$(nproc)" \
                        "EXTRA_CFLAGS=${EXTRA_CFLAGS:-}" \
                        "LDFLAGS=${LDFLAGS:-}" \
                    ; \
                    rm python ; \
                    make -j "$(nproc)" \
                        "EXTRA_CFLAGS=${EXTRA_CFLAGS:-}" \
                        "LDFLAGS=${LDFLAGS:--Wl},-rpath='\$\$ORIGIN/../lib'" \
                        python \
                    ; \
                    make DESTDIR=/build install \
                )
RUN         find /build/usr/local -depth \( \
                \( -type d -a \( \
                    -name test -o -name tests -o -name idle_test \
                \) \) -o \( \
                    -type f -a \( \
                        -name '*.pyc' -o -name '*.pyo' -o -name 'libpython*.a' \
                    \) \
                \) \
            \) -exec rm -rf '{}' + ;

ARG         ALPINE_VERSION
FROM        alpine:${ALPINE_VERSION}

LABEL       git.maintainer="Sloth De'vils" \
            git.repository="https://gitlab.com/freesrc/alpine-svd"

ENV         PYTHONDONTWRITEBYTECODE=1 \
            PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/.bin \
            TERM=xterm

COPY        --from=python-bld   /build/ /
COPY                            rootfs/ /

RUN         set -eux ;\
            find /usr/local -type f -executable -not \( -name '*tkinter*' \) \
                -exec scanelf --needed --nobanner --format '%n#p' '{}' ';' \
                | tr ',' '\n' \
                | sort -u \
                | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
                | xargs -rt apk add --no-cache --virtual .python-rundeps \
            ; \
            for src in idle3 pip3 pydoc3 python3 python3-config; do \
                dst="$(echo "$src" | tr -d 3)"; \
                [ -s "/usr/local/bin/$src" ]; \
                [ ! -e "/usr/local/bin/$dst" ]; \
                ln -svT "$src" "/usr/local/bin/$dst"; \
            done \
            ; \
            apk add --no-cache \
                    libc-utils tzdata shadow \
                    bash sed gawk tar \
            ; \
            python3 -m pip install -U \
                    pip \
                    supervisor ; \
            mkdir -p /opt /init.d/env /init.d/exec ; \
            crontab -u root -r ; \
            echo 'UID_MAX 10001' >> /etc/login.defs ; \
            groupadd \
                    -g 10001 \
                    runner \
            ; \
            useradd \
                    -u 10001 \
                    -g 10001 \
                    -G users \
                    -md /opt/runner \
                    -s /sbin/nologin \
                    runner \
            ; \
            rm -rf \
                    /var/cache/apk/* \
                    /tmp/* \
                    /var/tmp/*

ENTRYPOINT  ["/init"]

STOPSIGNAL  SIGTERM
