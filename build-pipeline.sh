#!/usr/bin/env bash
# shellcheck shell=bash disable=SC2034,SC2129,SC2154

set -x

dockerArch="linux/amd64,linux/arm64"

alpine=( "3.19" "3.20" "3.21" )
nginx="1.26.2"
php=( "8.2" "8.3" "8.4" )

exclude=( ['317']="8.0" ['318']="8.0" )

phpver_0="8.2.27"
phpver_1="8.3.17"
phpver_2="8.4.4"

for alpn in "${alpine[@]}";do
    cat <<EOL > "${alpn}"_base.yaml
alpine:${alpn}:base:
  stage: base
  variables:
    BUILD_ARCH: "${dockerArch}"
    ALPINE_VERSION: "${alpn}"
  script:
    - bash -c './build-docker.sh --target=prod --dist=base'
  timeout: 3 hour
  artifacts:
    name: "\$CI_JOB_NAME_SLUG"
    paths:
      - build.log
    expire_in: 3 days
    when: always

EOL

    cat <<EOL > "${alpn}"_nginx.yaml
alpine:${alpn}:nginx:
  stage: nginx
  variables:
    BUILD_ARCH: "${dockerArch}"
    ALPINE_VERSION: "${alpn}"
    NGINX_VERSION: "${nginx}"
  script:
    - bash -c './build-docker.sh --target=prod --dist=nginx'
  needs: ["alpine:${alpn}:base"]
  timeout: 5 hours
  artifacts:
    name: "\$CI_JOB_NAME_SLUG"
    paths:
      - build.log
    expire_in: 3 days
    when: always

EOL

    pv=0
    while [[ ${pv} -lt ${#php[@]} ]];do
        if ! [[ "${php[${pv}]}" == "${exclude[${alpn//\./}]}" ]];then
            eval phpver="\${phpver_${pv}}"
            cat <<EOL > "${alpn}"_fpm"${php[${pv}]}".yaml
alpine:${alpn}:fpm${php[$pv]}:
  stage: php-fpm
  variables:
    BUILD_ARCH: "${dockerArch}"
    ALPINE_VERSION: "${alpn}"
    PHP_VERSHRT: "${php[$pv]}"
    PHP_VERSION: "${phpver}"
  script:
    - bash -c './build-docker.sh --target=prod --dist=php-fpm'
  needs: ["alpine:${alpn}:nginx"]
  timeout: 8 hours
  artifacts:
    name: "\$CI_JOB_NAME_SLUG"
    paths:
      - build.log
    expire_in: 3 days
    when: always

EOL

            unset phpver
        fi
        pv=$((pv+1))
    done
done

cat <<EOL > build-pipeline.yaml
stages:
  - base
  - nginx
  - php-fpm

default:
  before_script:
    - apk add --no-cache bash
    - docker login -u "\$CI_REGISTRY_USER" -p "\$CI_REGISTRY_PASSWORD" \$CI_REGISTRY
    - docker context create tls-context
    - docker buildx create --bootstrap --config /etc/buildkitd.toml --name tlsma --platform ${dockerArch} --use tls-context
  after_script:
    - docker buildx rm -f tlsma

EOL

cat -- *_base.yaml  >> build-pipeline.yaml && rm -- *_base.yaml
cat -- *_nginx.yaml >> build-pipeline.yaml && rm -- *_nginx.yaml
cat -- *_fpm*.yaml  >> build-pipeline.yaml && rm -- *_fpm*.yaml
