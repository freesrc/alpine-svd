ARG         ALPINE_VERSION
FROM        alpine:${ALPINE_VERSION} AS build

ARG         PHP_VERSION

RUN         set -eux ; \
            apk add --no-cache --virtual .build-deps \
                    autoconf \
                    dpkg-dev dpkg \
                    file \
                    g++ \
                    gcc \
                    libc-dev \
                    make \
                    pkgconf \
                    re2c \
                    ca-certificates \
		            curl \
		            tar \
		            xz \
                    openssl \
                    gnupg \
                    argon2-dev \
                    coreutils \
                    curl-dev \
                    gnu-libiconv-dev \
                    libsodium-dev \
                    libxml2-dev \
                    linux-headers \
                    oniguruma-dev \
                    openssl-dev \
                    readline-dev \
                    sqlite-dev \
            ;
RUN         set -eux ; \
            rm -vf /usr/include/iconv.h
RUN         set -eux ; \
            ln -sv /usr/include/gnu-libiconv/*.h /usr/include/

ENV         PHP_INI_DIR="/usr/local/etc"

RUN         set -eux ; \
            mkdir -p \
                    /usr/src/php \
                    /build${PHP_INI_DIR}/php.conf.d \
                    /build/usr/src \
            ;
RUN         set -eux; \
            curl -fSL http://storage.svc.freesrc.ru/php/${PHP_VERSION}.tar.gz \
                 -o /tmp/php.tar.gz
RUN         set -eux; \
            echo "$(curl -fsSL http://storage.svc.freesrc.ru/php/${PHP_VERSION}.sum.sha256) */tmp/php.tar.gz" | sha256sum -c
RUN         set -eux; \
            tar -zxC /usr/src/php -f /tmp/php.tar.gz --strip-components=1

ARG         PHP_CFLAGS="-fstack-protector-strong -fpic -fpie -O2 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"
ARG         PHP_CPPFLAGS="$PHP_CFLAGS"
ARG         PHP_LDFLAGS="-Wl,-O1 -pie"
ARG         gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"

ENV         CONFIG=' \
                    --build="${gnuArch}" \
                    --with-config-file-path="${PHP_INI_DIR}" \
                    --with-config-file-scan-dir="${PHP_INI_DIR}/php.conf.d" \
                    --enable-option-checking=fatal \
                    --with-mhash \
                    --with-pic \
                    --enable-ftp \
                    --enable-mbstring \
                    --enable-mysqlnd \
                    --with-password-argon2 \
                    --with-sodium=shared \
                    --with-pdo-sqlite=/usr \
                    --with-sqlite3=/usr \
                    --with-curl \
                    --with-iconv=/usr \
                    --with-openssl \
                    --with-readline \
                    --with-zlib \
                    --disable-phpdbg \
                    --with-pear \
                    $(test "${gnuArch}" = "s390x-linux-musl" && echo "--without-pcre-jit") \
                    --disable-cgi \
                    --enable-fpm \
                    --with-fpm-user=runner \
                    --with-fpm-group=runner \
                ' \
            CFLAGS="${PHP_CFLAGS}" \
            CPPFLAGS="${PHP_CPPFLAGS}" \
            LDFLAGS="${PHP_LDFLAGS}"

RUN         set -eux; \
            ( \
                cd /usr/src/php ; \
                ./configure $(eval echo $(eval echo ${CONFIG})) ; \
                make -j"$(nproc)" ; \
                find -type f -name '*.a' -delete ; \
                make INSTALL_ROOT=/build install ; \
                find \
		            /build/usr/local \
		            -type f \
		            -perm '/0111' \
		            -exec sh -euxc ' \
			            strip --strip-all "$@" || : \
		            ' -- '{}' + \
	            ; \
                cp -v php.ini-* /build${PHP_INI_DIR}/ ; \
                # cp -v /tmp/php.tar.gz /build/usr/src  ; \
            )
RUN         rm -rf /build/var/run

ARG         ALPINE_VERSION
FROM        registry.gitlab.com/freesrc/alpine-svd:${ALPINE_VERSION}-ngx

COPY        --from=build /build/    /
COPY                     rootfs.fpm /

RUN         set -ex; \
            fpmDeps="$( \
		        scanelf --needed --nobanner --format '%n#p' --recursive /usr/local \
			        | tr ',' '\n' \
			        | sort -u \
			        | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	        )"; \
            apk add --no-cache \
                    ca-certificates \
                    xz \
                    binutils \
                    $fpmDeps ; \
            pecl update-channels ; \
            php-ext-enable sodium ; \
            rm -rf \
                    /tmp/pear \
                    ~/.pearrc \
                    /var/cache/apk/* \
                    /tmp/* \
                    /var/tmp/* \
            ; \
            php --version

STOPSIGNAL  SIGQUIT

EXPOSE      9000
