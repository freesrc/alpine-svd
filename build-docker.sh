#!/usr/bin/env bash
# shellcheck shell=bash disable=SC2086
# set -x
exec &> >(tee -a build.log)

usage() {
    echo 'there is no hope...'
    exit 1
}

__buildDocker() {
    docker "${__buildArgs[@]}" --tag ${image}:${__buildTags} --file ${__buildFile} .
}

__base() {
    [[ -z "${ALPINE_VERSION}" ]] \
        && echo "ALPINE_VERSION is empty" && exit 1
    __buildTags="${ALPINE_VERSION}"
    __buildArgs+=( "--build-arg" "ALPINE_VERSION=${ALPINE_VERSION}" )
}
__nginx() {
    [[ -z "${NGINX_VERSION}" ]] \
        && echo "NGINX_VERSION is empty" && exit 1
    __base
    __buildFile="${__buildFile}.ngx"
    __buildTags="${__buildTags}-ngx"
    __buildArgs+=( "--build-arg" "NGINX_VERSION=${NGINX_VERSION}" )
}
__php-fpm() {
    [[ -z "${PHP_VERSION}" || -z "${PHP_VERSHRT}" ]] \
        && echo "PHP_VERSION is empty" && exit 1
    __base
    __buildFile="${__buildFile}.fpm"
    __buildTags="${__buildTags}-fpm${PHP_VERSHRT}"
    __buildArgs+=( "--build-arg" "PHP_VERSION=${PHP_VERSION}" )
}

variants=( "base" "nginx" "php-fpm" )
dist='base'
image='registry.gitlab.com/freesrc/alpine-svd'
__buildTags=''
__buildArgs=( "buildx" "build" "--platform" "${BUILD_ARCH}" )
__buildFile='Dockerfile'

SOPT='d:t:'
LOPT='dist:,target:'
if ! OPTS=$(getopt -q -a \
    --options ${SOPT} \
    --longoptions ${LOPT} \
    --name "$(basename "$0")" \
    -- "$@"
); then
    exit 2
fi

eval set -- "$OPTS"

while [[ $# -gt 0 ]]; do
    case ${1} in
        -d|--dist) dist=$2 && shift   ;;
        -t|--target)
            case ${2} in
                'prod')
                    __buildArgs+=( "--no-cache" "--pull" "--push" "--compress" "--rm" )
                ;;
                'dev')
                    __buildArgs+=( "--load" "--progress" "plain" )
                ;;
            esac
            shift
        ;;
        --)                   break   ;;
        *)                    usage   ;;
    esac
    shift
done

[[ -z ${dist} ]] && usage

if [[ " ${variants[*]} " =~ ${dist} ]];then
    eval __${dist}
    __buildDocker
fi
